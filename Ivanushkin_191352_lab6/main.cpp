#include <iostream>
using namespace std;

void inputMatrix(int*& matr, int* matr_col, int* matr_rows)
{
    cout << "Input size of matrix: ";
    cin >> *matr_rows >> *matr_col;
    matr = new int[(*matr_col) * (*matr_rows)];
    for (int i = 0; i < (*matr_col) * (*matr_rows); i++)
    cin >> matr[i];
}

    void printMatrix(int* matr, int matr_col, int matr_rows)
{
    for (int i = 0; i < matr_rows; i++)
{
    for (int j = 0; j < matr_col; j++)
{
    cout << matr[i * matr_col + j] << "\t";
}
    cout << std::endl;
}
}

    void matrixsum(int* mat1, int mat1col, int mat1rows,
    int* mat2, int mat2col, int mat2rows,
    int*& mat3, int* mat3col, int* mat3rows)
{
    if (mat1col == mat2col && mat1rows == mat2rows)
{
    *mat3rows = mat2rows;
    *mat3col = mat2col;
    mat3 = new int[mat2rows * mat2col];
    for (int i = 0; i < mat2rows * mat2col; i++)
                //mat3[i,j] = mat1[i, j]+mat2[i,j]
    mat3[i] = mat1[i] + mat2[i];
}
    else
{
    cout << "Error of sum: sizes are not equal.";
    *mat3col = *mat3rows = 0;
}
}

    void matrixmultiplacationbynumber(int* mat1, int mat1col, int mat1rows,
    int*& mat3, int* mat3col, int* mat3rows, int number) {

    *mat3rows = mat1rows;
    *mat3col = mat1col;
    mat3 = new int[mat1rows * mat1col];
    for (int i = 0; i < mat1rows * mat1col; i++)
    mat3[i] = mat1[i] * number;
}

    void diagonal_sum(int* mat1, int mat1col, int mat1rows) {

    int p = 0;
    int sum = 0;

    if (mat1rows == mat1col) {

    for (int i = 0; i < mat1col; i++) {

    p = mat1[i + mat1col * i];
    sum += p;\
    p = 0;

}

    cout << sum;

}

    else
{
    cout << "Error";
}

}
    void matrixxmatrix(int* mat1, int mat1col, int mat1rows,
    int* mat2, int mat2col, int mat2rows,
    int*& mat3, int* mat3col, int* mat3rows) {
    if (mat1col == mat2rows)
{
    *mat3rows = mat1rows;
    *mat3col = mat2col;
    mat3 = new int[mat1rows * mat2col];

    for (int i = 0; i < mat1rows; i++) {
    for (int j = 0; j < mat2col; j++) {
    mat3[i * (*mat3col) + j] = 0;
    for (int k = 0; k < mat1col; k++) {
    //mat3[i,j]+=mat1[i,k]*mat2[k,j]
    mat3[i * (*mat3col) + j] += mat1[i * (mat1col)+k] * mat2[k * (mat2col)+j];
}
}
}
}
}

    void task1() {

    int* matr1;
    int* matr2;
    int* matrres;
    int matr1_col, matr2_col, matrres_col;
    int matr1_rows, matr2_rows, matrres_rows;
    inputMatrix(matr1, &matr1_col, &matr1_rows);
    inputMatrix(matr2, &matr2_col, &matr2_rows);
    matrixsum(matr1, matr1_col, matr1_rows,
    matr2, matr2_col, matr2_rows,
    matrres, &matrres_col, &matrres_rows);

    printMatrix(matrres, matrres_col, matrres_rows);
    delete[] matr1;
    delete[] matr2;
    delete[] matrres;

}

    void task2() {

    int* matr1;
    int* matrres;
    int matr1_col, matrres_col;
    int matr1_rows, matrres_rows;
    int xdigit;
    inputMatrix(matr1, &matr1_col, &matr1_rows);
    cout << "Input digit: ";
    cin >> xdigit;
    matrixmultiplacationbynumber(matr1, matr1_col, matr1_rows,
    matrres, &matrres_col, &matrres_rows,
    xdigit);
    printMatrix(matrres, matrres_col, matrres_rows);
    delete[] matr1;
    delete[] matrres;

}

    void task4() {

    int* matr1;
    int matr1_col;
    int matr1_rows;

    inputMatrix(matr1, &matr1_col, &matr1_rows);

    diagonal_sum(matr1, matr1_col, matr1_rows);

    delete[] matr1;

}
    void task5() {
    int* matr1;
    int* matr2;
    int* matrres;
    int matr1_col, matr2_col, matrres_col;
    int matr1_rows, matr2_rows, matrres_rows;
    inputMatrix(matr1, &matr1_col, &matr1_rows);
    inputMatrix(matr2, &matr2_col, &matr2_rows);

    matrixxmatrix(matr1, matr1_col, matr1_rows,
    matr2, matr2_col, matr2_rows,
    matrres, &matrres_col, &matrres_rows);

    printMatrix(matrres, matrres_col, matrres_rows);

    delete[] matr1;
    delete[] matr2;
    delete[] matrres;
}

    int main()
{

    int prog = 0;
    cin >> prog;

    switch (prog) {
    case(1): {
    task1();
    break;
}
    case(2): {
    task2();
    break; }
    case(3): {
    task5();
    break; }
    case(4): {
    task4();
    break; }

}
}
