#pragma once
#include <iostream>
#include "matrix.h"

using namespace std;

class dsquare :
    public matrix
{

private:

    int rows;
    int columns;
    int* arr;

public:

    dsquare();
    ~dsquare();

    dsquare(int t_rows, int  t_col);
    dsquare(int t_rows, int t_col, int* mas);

    void inputS();
    void printS();
};
