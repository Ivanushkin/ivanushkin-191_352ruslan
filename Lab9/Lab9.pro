TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        dsquare.cpp \
        main.cpp \
        matrix.cpp \
        vector.cpp

HEADERS += \
    dsquare.h \
    matrix.h \
    vector.h
