#include "dsquare.h"
#include <iostream>
#include "matrix.h"

using namespace std;


dsquare::dsquare()
{
}

dsquare::~dsquare()
{
}

dsquare::dsquare(int t_rows, int t_col)
{
    columns = t_col;
    rows = t_col;
    arr = new int[rows * columns];
}

dsquare::dsquare(int t_rows, int t_col, int* mas)
{
    rows = t_col;
    columns = t_col;
    arr = new int[rows * columns];
    for (int i = 0; i < rows * columns; i++) {
        arr[i] = mas[i];
    }

}

void dsquare::inputS()
{
    cout << "Input size of matrix: ";
    cin >> rows;
    columns = rows;
    arr = new int[rows * columns];

    for (int i = 0; i < (columns * rows); i++) {
        arr[i] = 0;
    }

    for (int j = 0; j < columns; j++) {
        cin >> arr[j + columns * j];
    }
};

void dsquare::printS()
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            cout << arr[i * rows + j] << "\t";
        }
        cout << endl;
    }
}
