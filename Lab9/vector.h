#pragma once
#include "matrix.h"
#include <iostream>

using namespace std;

class vector :
    public matrix
{

private:

public:

    vector();
    ~vector();

    vector(int t_columns) : matrix(1, t_columns) {} ;
    vector(int t_columns, int* mas) : matrix(1, t_columns, mas) {};

    void inputV();
    void printV();

    void scalnum(const vector& vec2);
    void length();

};
