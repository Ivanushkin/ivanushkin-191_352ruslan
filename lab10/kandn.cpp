#include "KandN.h"
#include <iostream>
#include <string>

using namespace std;

KaN::KaN()
{
    for (int i = 0; i < 9; i++) {
        tabl[i] = ' ';
    }
}

KaN::~KaN()
{
}

void KaN::printactualpos()
{
    int j = 0;
    for (int i = 0; i < 9; i++) {

        cout << "|" << tabl[i] << "|";
        j++;
        if (j == 3) {
            cout << endl;
            j = 0;
        }
    }
}

void KaN::restart()
{
    for (int i = 0; i < 9; i++) {
        tabl[i] = ' ';
    }
}

void KaN::cancellastturn()
{
}

void KaN::whoseturn()
{
    if (flag == true) {
        player = 'X';
    }
    else {
        player = 'O';
    }
}

bool KaN::testendgame()
{
    if (tabl[0] == 'X' && tabl[1] == 'X' && tabl[2] == 'X' ||
        tabl[0] == 'O' && tabl[1] == 'O' && tabl[2] == 'O' ||

        tabl[3] == 'X' && tabl[4] == 'X' && tabl[5] == 'X' ||
        tabl[3] == 'O' && tabl[4] == 'O' && tabl[5] == 'O' ||

        tabl[6] == 'X' && tabl[7] == 'X' && tabl[8] == 'X' ||
        tabl[6] == 'O' && tabl[7] == 'O' && tabl[8] == 'O' ||

        tabl[0] == 'X' && tabl[3] == 'X' && tabl[6] == 'X' ||
        tabl[0] == 'O' && tabl[3] == 'O' && tabl[6] == 'O' ||

        tabl[1] == 'X' && tabl[4] == 'X' && tabl[7] == 'X' ||
        tabl[1] == 'O' && tabl[4] == 'O' && tabl[7] == 'O' ||

        tabl[2] == 'X' && tabl[5] == 'X' && tabl[8] == 'X' ||
        tabl[2] == 'O' && tabl[5] == 'O' && tabl[8] == 'O' ||

        tabl[0] == 'X' && tabl[4] == 'X' && tabl[8] == 'X' ||
        tabl[0] == 'O' && tabl[4] == 'O' && tabl[8] == 'O' ||

        tabl[2] == 'X' && tabl[4] == 'X' && tabl[6] == 'X' ||
        tabl[2] == 'O' && tabl[4] == 'O' && tabl[6] == 'O'
        )
    {
        cout << player << " win!" << endl;
        return  0;
    }
    if (flag == true) {
        flag = false;
    }
    else {
        flag = true;
    }
}


void KaN::turn()
{
    whoseturn();
    printactualpos();
    string trn;
    cout << "Turn " << player << endl;
    cin >> trn;
    if (trn == "restart" || trn == "Restart") {
        restart();
        return turn();
    }
    else if (trn == "whose" || trn == "Whose") {
        cout << player << endl;;
        return turn();
    }
    else if (trn == "1" || trn == "2" || trn == "3" || trn == "4" || trn == "5" || trn == "6" || trn == "7" || trn == "8" || trn == "0") {
        int turn1 = atoi(trn.c_str());
        if (tabl[turn1] == ' ' ) {
            tabl[turn1] = player;
        }
        else {
            cout << "Choose empty cell" << endl;
            return turn();
        }
    }
    else {
        cout << " Incorrect input " << endl;
        return turn();
    }

}

void KaN::testturn()
{
}
