#pragma once

#include <iostream>
#include <string>

using namespace std;

class KaN
{
private:

    char tabl[9];
    bool flag = false;
    char player;

public:

    KaN();
    ~KaN();

    void printactualpos();

    void restart();

    void turn();

    void testturn();

    void whoseturn();

    void cancellastturn();


    bool testendgame();


};
