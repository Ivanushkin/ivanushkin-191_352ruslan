#include <iostream>
using namespace std;

void sortbycounting(char* mas, int n) {
    int counterarray[26] = { 0 };
    for (int i = 0; i < n; i++) {
        //'a' <=mas[i] <='z'
        //int('a')<= int(mas[i]) <= int('z')
        //int('a')-int('a') <= int(mas[i])-int('a') <= int('z') -int('a')
        // 0 <= int(mas[i])-int('a') <= 25
        counterarray[int(mas[i]) - int('a')]++;
    }
    int j = 0;//counter for mas;
    for (int i = 0; i < 26; i++) {
        if (counterarray[i] > 0) {
            for (int k = 0; k < counterarray[i]; k++) {
                mas[j] = char(int('a') + i);
                j++;
            }
        }
    }
}

void printarr(int* mas, int size) {
    cout << endl;
    for (int p = 0; p < size; p++) {
        cout << "arr[" << p << "]=" << mas[p] << endl;
    }
}

void princhararray(char* mas, int n) {
    for (int i = 0; i < n; i++) {
        cout << mas[i] << " ";
    }
    cout << endl;
}

void sort(int* massive, int size) {
    for (int c = 0; c < size; c++) {
        for (int k = c; k < size; k++) {
            if (massive[c] > massive[k]) {
                int g = massive[c];
                massive[c] = massive[k];
                massive[k] = g;
            }
        }
    }
    printarr(massive, size);

}

int digitsum(int n) {
    int res = 0;
    while (n > 0) {
        res += n % 10;
        n = n/10;
    }
    return res;
}

void sortbysum(int* massive, int size) {
    for (int c = 0; c < size; c++) {
        for (int k = c; k < size; k++) {
            if (digitsum(massive[c]) < digitsum(massive[k]))
            {
                int g = massive[c];
                massive[c] = massive[k];
                massive[k] = g;
            }
        }
    }
    for (int i = 0; i < 5; i++) {
        cout << massive[i]<<" ";
    }
    cout << endl;

}


void task1()
{
    //input array

    int size = 0;
    char arr[100] = { ' ' };
    cout << "Input size of array: ";
    cin >> size;
    for (int i = 0; i < size; i++) {
        cin >> arr[i];
    }

    //sorting
    sortbycounting(arr, size);


    //print result
    princhararray(arr, size);
}

void task2() {
    int arr[5];
    cout << "Input array of 5 numbers";
    for (int i = 0; i < 5; i++){
        cin >> arr[i];
    }
    sortbysum(arr, 5);
    for (int i = 0; i < 5; i++) {
        cout << arr[i];
    }

}


int main() {
    cout << "What do you want? \n"
        "1. Sort letters.\n"
        "2. Sort by sum of digits.\n"
        "3. Exit.\n";
    int prog = 0;
    cin >> prog;
    switch (prog) {
    case(1): {
        task1();
        break;
    }

    case(2): {
        task2();
        break;
    }

    default: {
        return 0; }
    }
}
