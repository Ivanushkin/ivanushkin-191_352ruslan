#include <cstring>
#include <sstream>
#include <string>
#include <iostream>
using namespace std;

void countAndSay(int n) {

    cout << endl;
    string result = "1";

    if (n == 0) {
        cout << "" << endl;
    }
    else {
        cout << result << endl;
    }

    string temp;
    for (size_t i = 1; i < n; i++) {

        size_t len = result.length();
        for (int j = 0; j < len; j++) {
            int count = 1;
            while (j + 1 < len && result[j] == result[j + 1]) {
                count++;
                j++;
            }

            temp += to_string(count) + result[j];
        }

        result = temp;
        cout << result << endl;

        temp = "";

    }
}

int main() {
    int n;
    cout << " Enter n, number of rows\n";
    cin >> n;
    countAndSay(n);

    return 0;
}

