#pragma once


#include <iostream>
using namespace std;

template <class type>

void printMatrix(int* matr, int matr_col, int matr_rows);


class matrix
{

private:
    int rows;
    int columns;
    int* arr;

public:

    matrix();
    ~matrix();

    matrix(int t_rows, int t_columns);
    matrix(int t_rows, int t_columns, int* mas);

    //sum
    matrix& matrixsum(const matrix& mat2);

    //mult
    matrix& matrixmult(const matrix& mat2);

    //multnum
    matrix& matrixmultnum(int number);

    //trace
    matrix& matrixtrace(const matrix& mat);

    //det

    //print

    void print();

    //input

    void input();

    matrix& operator += (const matrix& mat);

    matrix& operator -= (const matrix& mat);

    //peregruz +
    friend matrix& operator + (const matrix& mat1, const matrix& mat2);

    //peregruz -
    friend matrix& operator - (const matrix& mat1, const matrix& mat2);

    //peregruz *num
    friend matrix& operator * (const matrix& mat1, const int num);

    //peregruz *mat
    /*friend matrix& operator * (const matrix& mat1, const matrix& mat2);*/

    //peregruz <<
    friend ostream& operator << (ostream& out, const matrix& mat);

    //peregruz >>
    friend istream& operator >> (istream& in, matrix& mat);
};

inline matrix& operator + (const matrix& mat1, const matrix& mat2) {

    if (mat1.rows == mat2.rows && mat1.columns == mat2.columns) {
        int* res;
        res = new int[mat1.columns * mat1.rows];
        for (int i = 0; i < mat1.rows * mat1.columns; i++) {
            res[i] = mat1.arr[i] + mat2.arr[i];
        }
        matrix temp(mat1.rows, mat1.columns, res);
        return temp;
    }
    matrix temp(0, 0, nullptr);
    return temp;
}

inline matrix& operator - (const matrix& mat1, const matrix& mat2) {

    if (mat1.rows == mat2.rows && mat1.columns == mat2.columns) {
        int* res;
        res = new int[mat1.columns * mat1.rows];
        for (int i = 0; i < mat1.rows * mat1.columns; i++) {
            res[i] = mat1.arr[i] - mat2.arr[i];
        }
        matrix temp(mat1.rows, mat1.columns, res);
        return temp;
    }
    matrix temp(0, 0, nullptr);
    return temp;
}

inline matrix& operator * (const matrix& mat1, const int num) {

        int* res;
        res = new int[mat1.columns * mat1.rows];
        for (int i = 0; i < mat1.rows * mat1.columns; i++) {
            res[i] = mat1.arr[i] * num;
        }
        matrix temp(mat1.rows, mat1.columns, res);
        return temp;
}

//inline matrix& operator * (const matrix& mat1, const matrix& mat2) {
//
//	if (mat1.columns == mat2.rows)
//	{
//		int* res;
//		*resrows = mat1.rows;
//		*rescolumns = mat2.columns;
//		res = new int[mat1.rows * mat2.columns];
//
//		for (int i = 0; i < mat1.rows; i++) {
//			for (int j = 0; j < mat2.columns; j++) {
//				res[i * (*mat2.columns) + j] = 0;
//				for (int k = 0; k < mat1.columns; k++) {
//					//mat3[i,j]+=mat1[i,k]*mat2[k,j]
//					res[i * (*rescolumns) + j] += mat1.arr[i * (mat1.columns) + k] * mat2.arr[k * (mat2.columns) + j];
//				}
//			}
//		}
//		matrix temp(mat1.rows, mat2.columns, res);
//		return temp;
//	}
//	matrix temp(0, 0, nullptr);
//	return temp;
//}

inline ostream& operator << (ostream &out, const matrix &mat) {

    for (int i = 0; i < mat.rows; i++)
    {
        for (int j = 0; j < mat.columns; j++)
        {
            out << mat.arr[i * mat.columns + j] << "\t";
        }
        out << endl;
    }
    return out;
}

inline istream& operator >> (istream& in, matrix& mat) {

    cout << "Input size of matrix: \n"
        "rows = ";
    in >> mat.rows;
    cout << "columns = ";
    in >> mat.columns;
    mat.arr = new int[mat.rows * mat.columns];


    for (int i = 0; i < (mat.columns * mat.rows); i++)
        in >> mat.arr[i];
        return in;
}
