

#include "matrix.h"
#include <iostream>
using namespace std;


void printMatrix(int* matr, int matr_col, int matr_rows)
{
    for (int i = 0; i < matr_rows; i++)
    {
        for (int j = 0; j < matr_col; j++)
        {
            cout << matr[i * matr_col + j] << "\t";
        }
        cout << endl;
    }
}


matrix::matrix()
{
}
matrix::~matrix()
{
}


matrix::matrix(int t_rows, int t_columns)
{
    rows = t_rows;
    columns = t_columns;
    arr = new int[rows * columns];
}

matrix::matrix(int t_rows, int t_columns, int* mas)
{
    rows = t_rows;
    columns = t_columns;
    arr = new int[rows * columns];
    for (int i = 0; i < rows * columns; i++) {
        arr[i] = mas[i];
    }

}

matrix& matrix::matrixsum(const matrix& mat2)
{
    if (columns == mat2.columns && rows == mat2.rows)
    {
        int resrows = rows;
        int rescolumns = columns;
        matrix res(resrows, rescolumns);
        //int* mat3 = new int[resrows * rescolumns];
        for (int i = 0; i < resrows * rescolumns; i++)
            //mat3[i,j] = mat1[i, j]+mat2[i,j]
            res.arr[i] = arr[i] + mat2.arr[i];
            //mat3[i] = arr[i] + mat2.arr[i];
        return res;
    }
    else
    {
        matrix res(0, 0);
        return res;
    }

}

matrix& matrix::matrixmult(const matrix& mat2)
{
    if (rows == mat2.columns)
    {

        int resrows = rows;
        int rescolumns = mat2.columns;
        matrix res(resrows, rescolumns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < mat2.columns; j++) {
                for (int k = 0; k < columns; k++) {
                    //mat3[i,j]+=mat1[i,k]*mat2[k,j]
                    res.arr[i * (rescolumns)+j] += arr[i * (columns)+k] * mat2.arr[k * (mat2.columns) + j];
                }
            }
        }
        return res;
    }
    else
    {
        matrix res(0, 0);
        return res;
    }
}

matrix& matrix::matrixmultnum(int number)
{
    int resrows = rows;
    int rescolumns = columns;
    matrix res(resrows, rescolumns);
    for (int i = 0; i < resrows * rescolumns; i++)
        res.arr[i] = arr[i] * number;
    return res;

}

matrix& matrix::matrixtrace(const matrix& mat)
{
    int sum = 0;
    int p = 0;

    if (mat.rows == mat.columns) {

        for (int i = 0; i < mat.columns; i++) {

            p = mat.arr[i + mat.columns * i];
            sum += p;
            p = 0;

        }

    }

    else
    {
        cout << "Error";
    }

    matrix res(1, 1);
    res.arr[1] = sum;
    return res;
}


void matrix::print()
{
    printMatrix(arr, columns, rows);
}

void matrix::input()
{
    cout << "Input size of matrix: \nrows = ";
    cin >> rows;
    cout << "columns = ";
    cin	>> columns;
    arr = new int[rows * columns];


    for (int i = 0; i < (columns * rows); i++)
        cin>> arr[i];
}

matrix& matrix::operator+=(const matrix& mat)
{
    if (rows = mat.rows && columns == mat.columns) {
        for (int i = 0; i < rows * columns; i++) {
            arr[i] += mat.arr[i];

        }
        return *this;
    }
}

matrix& matrix::operator-=(const matrix& mat)
{
    if (rows = mat.rows && columns == mat.columns) {
        for (int i = 0; i < rows * columns; i++) {
            arr[i] -= mat.arr[i];

        }
        return *this;
    }
}
